======================
 Traefik Remote Proxy
======================


Overview
--------
Hosting things on the internet is great. The advent of cheap cloud providers
like Digital Ocean, Linode, Amazon EC2, and whatever else has made it really
cheap and easy to get your stuff out on the internet. The tradeoff for many
of us is not being able to use any physical hardware we may have in our
homes. You could of course expose the devices in your home directly to the
internet, but then you have to work around your ISP blocking ports...and you
also invite internet spam bots to scan your house and to try to login with some
default username and a plethora of defaults passwords. It would be great
to be able to get the best parts of both hosting things in the cloud and
hosting things in your house.


Requirements
------------
- A VPS in the cloud running some sort of Linux
- A Linux Server on your home network
- A domain name
- Basic Linux knowledge
- Some basic understanding of Docker and ideally Docker Compose

Tools/Services
--------------
- Traefik
- Tinc
- Docker
- Docker Compose
- Let's Encrypt

Part 1: Getting Started
-----------------------
Starting here, I'll assume you have:

- A domain: I'll be using ``example.com`` is this tutorial, remember to substitute
  in your domain as needed
- A VPS that you can SSH into: I'll be using ``128.66.0.1`` as this server's IPV4
  address, remember to substitute in your server's real IP as needed
- A home server that you can SSH into

Setting up DNS
______________
The first thing that we will want to do is setup the DNS records that we will
need. This will require you to pick a hostname for both your cloud and local
server. In this tutorial I'm going to use ``cloud`` as the hostname for the
cloud server and ``home`` as the hostname for the home server, but I suggest
you pick something more fun and substitute it in as needed.


We will want to create two DNS records, one for each server.

.. warning::

    DNS records can take >15-30 min to show up after you create them, so make
    sure to be patient if they aren't working right away

The first of these records is an A record for our cloud server, pointing to its
ip address.

The resulting output of dig should be:

.. code-block:: bash

    $ dig cloud.example.com
    ...
    cloud.example.com.    172817    IN     A         128.66.0.1
    ...

The second of these records is a CNAME record for our home server, pointing to
the A record for ``cloud.example.com``.

The resulting output of dig should be:

.. code-block:: bash

    $ dig home.example.com
    ...
    home.example.com.     172817    IN     CNAME    cloud.example.com
    cloud.example.com.    172817    IN     A        128.66.0.1
    ...


After you have these two records added, you are done configuring the required
DNS records.

You may also want to add a CNAME record for just ``example.com``
to also point to ``cloud.example.com`` so you can host things at the root
of your domain. I left it as a side option in this tutorial because sometimes
people like to point the root domain at dedicated web hosting and I wanted
to make sure this would be easily compatible with that.

Setting Up The Linux Servers
______________________________

This tutorial is designed to be easily transferable to your Linux distribution
of choice. Because of this, you'll have to figure out a few things yourself.

You'll need to figure out how to install Docker on your cloud server. Ideally
I'd recommend you try to install Docker from your distribution's package repos,
but you're welcome to use any method you like, they all should be compatible
with this tutorial. There are a ton of tutorials on getting this setup so you
should be able to figure it out without too much trouble.

You'll also want to install the Docker Compose utility alongside Docker.

From here on out, I'll assume you have Docker and Docker Compose installed on
your cloud and home servers. 

Part 2: Setting Up Your Cloud Server
-------------------------------------------

Setting up Traefik
___________________

The first thing we are going to get setup on the cloud server is Traefik.
Traefik is, in the words of the Traefik project, "a leading modern reverse
proxy and load balancer that makes deploying microservices easy. Traefik
integrates with your existing infrastructure components and configures
itself automatically and dynamically."

Now that's a whole lot of fancy marketing speak. For our purposes, Traefik
is just a reverse proxy with a bunch of cool fancy features that make
integrating it with Docker very easy.

We're going to use Docker Compose to setup an instance of Traefik. If you
haven't used it before, Docker Compose is a really nice tool that allows
us to define sets of Docker containers in yaml files and manage them with
ease.

The first thing you want to do is create a directory for us to work in.
I would suggest naming it ``docker-compose`` or something similar. I like
to create sub-directories under this folder for each service I'm running
using Docker Compose. Inside this ``docker-compose`` folder you'll now
want to create a ``traefik`` folder.

Now that you have this directory structure setup, we can start configuring
your services. 

Launch The Docker Daemon
||||||||||||||||||||||||

In order to continue with this tutorial, we will need to launch the Docker
daemon on your system. On systemd based systems, this can usually be done
with the following command

.. code-block:: bash

   systemctl start docker

You will need to run this with root privileges.

If this command doesn't do the job or you system is not using systemd,
you may need to do something different. You should be able to look up
your distribution's specific process for launching the Docker daemon
online.

You may also want to configure the Docker daemon to launch at boot.
Under systemd, this can be done with the following

.. code-block:: bash

   systemctl enable docker

The same distribution gotchas apply as with the previous command.

Create Docker Network
|||||||||||||||||||||

The first thing we will do is create a Docker network. A Docker network
in a virtual network that docker containers can use to communicate
among themselves. This is very useful for containing and managing our
reverse proxy connections. This network can be created with the following
command

.. code-block:: bash

   docker network create proxy

Note that the ``docker`` command must be run by either a user in the
``docker`` group or the root user.

``docker-compose.yaml`` Configuration
|||||||||||||||||||||||||||||||||||||

First, inside this traefik folder you will want to create a file called
``docker-compose.yaml``. The contents of the file should be as follows:

.. code-block:: yaml

    version: '3'
    
    services:
      traefik:
        image: traefik:latest
        container_name: traefik
        restart: unless-stopped
        networks:
          - proxy
        ports:
          - 80:80
          - 443:443
        volumes:
          - /etc/localtime:/etc/localtime:ro
          - /var/run/docker.sock:/var/run/docker.sock:ro
          - ./data/traefik.yml:/traefik.yml:ro
          - ./data/home.yml:/home.yml:ro
          - ./data/acme.json:/acme.json
        labels:
          - "traefik.enable=true"
          - "traefik.http.routers.traefik.entrypoints=http"
          - "traefik.http.routers.traefik.rule=Host(`cloud.example.com`)"
          - "traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https"
          - "traefik.http.routers.traefik.middlewares=traefik-https-redirect"
          - "traefik.http.middlewares.traefik-auth.basicauth.users=username:htpasswd_data"
          - "traefik.http.routers.traefik-secure.entrypoints=https"
          - "traefik.http.routers.traefik-secure.rule=Host(`cloud.example.com`)"
          - "traefik.http.routers.traefik-secure.middlewares=traefik-auth"
          - "traefik.http.routers.traefik-secure.tls=true"
          - "traefik.http.routers.traefik-secure.tls.certresolver=http"
          - "traefik.http.routers.traefik-secure.service=api@internal"
    
    networks:
      proxy:
        external: true


Let's go over what each part of this file is doing.


First we set the ``docker-compose.yaml`` file version to 3.

.. code-block:: yaml

    version: '3'

Next we configure the container image, name and restart policy. If the
image doesn't specify a place to pull it from, docker will default to
dockerhub. The ``container_name`` can be changed as needed if you want
something more descriptive. The restart policy ``unless-stopped`` tells
docker to restart the container if it stops or we reboot the system unless
we explicitly stop it with ``docker stop`` or ``docker-compose down``.

.. code-block:: yaml
  
    services:
      traefik:
        image: traefik:latest
        container_name: traefik
        restart: unless-stopped


This is the networking configuration. First we attach the ``proxy`` network
to the container (we'll discuss this later). Next we forward port ``80`` and
``443`` into the container. These are the standard http and https ports and
we are going to have traefik handle all traffic going into and out of them.

.. code-block:: yaml

        networks:
          - proxy
        ports:
          - 80:80
          - 443:443

We will use Docker volumes to attach needed files into the docker container.
These files include both configuration files for Traefik and some needed
system files.

- The ``traefik.yml`` will configure some standard configuration for Traefik.
- The ``home.yml`` will provide our VPN tunnel reverse proxy rules to Traefik.
- We map ``/etc/localtime`` into the container as read only to allow Traefik
  to know our time zone.
- We map ``/var/run/docker.sock`` into the container as read only to allow
  Traefik to introspect into containers that are running on our Docker daemon
- The ``acme.json`` file is the file Traefik will use to store TLS keys
  receieved from Let's Encrypt.

.. code-block:: yaml

           volumes:
          - /etc/localtime:/etc/localtime:ro
          - /var/run/docker.sock:/var/run/docker.sock:ro
          - ./data/traefik.yml:/traefik.yml:ro
          - ./data/home.yml:/home.yml:ro
          - ./data/acme.json:/acme.json


Finally we need to add the required labels to enable the Traefik web interface.
These labels are a preview of what we will be setting up later, but for now
we won't dive into them.

One setting that we will need to set is the
``traefik.http.middlewares.traefik-auth.basicauth.users`` field. This field
sets the login information for the Traefik Web UI. The ``username`` field can be
set to anything you want. The ``htpasswd_data`` field contains a hashed version
of the password you want to use to log into the Traefik WebUI. This hashed version
can be generated using the ``htpasswd`` command line utility or any one of the
browser implementations. Note that when entering the hashed password in the
``docker-compose`` file, any $s need to be escaped by appending a second $.
For example, ``aa$b$mb$c`` would become ``aa$$b$$mb$$c``.

.. code-block:: yaml

           labels:
          - "traefik.enable=true"
          - "traefik.http.routers.traefik.entrypoints=http"
          - "traefik.http.routers.traefik.rule=Host(`cloud.example.com`)"
          - "traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https"
          - "traefik.http.routers.traefik.middlewares=traefik-https-redirect"
          - "traefik.http.middlewares.traefik-auth.basicauth.users=username:htpasswd_data"
          - "traefik.http.routers.traefik-secure.entrypoints=https"
          - "traefik.http.routers.traefik-secure.rule=Host(`cloud.example.com`)"
          - "traefik.http.routers.traefik-secure.middlewares=traefik-auth"
          - "traefik.http.routers.traefik-secure.tls=true"
          - "traefik.http.routers.traefik-secure.tls.certresolver=http"
          - "traefik.http.routers.traefik-secure.service=api@internal"


Traefik Main Configuration File
|||||||||||||||||||||||||||||||

Next we will need to create some of the files that we are mounting into the
traefik container. The first thing you will need to do is create a ``data``
folder that we will use to store the rest of the Traefik configuration.

Now that we made the ``data`` folder we can create the required traefik
configuration files.

First we want to create ``traefik.yml``. The following is the full contents
of the file. We will go over this bit-by-bit afterwards.


.. code-block:: yaml

    api:
      dashboard: true

    serversTransport:
      insecureSkipVerify: true

    entryPoints:
      http:
        address: ":80"
      https:
        address: ":443"

    providers:
      docker:
        endpoint: "unix:///var/run/docker.sock"
        exposedByDefault: false
      file:
        filename: home.yml
        watch: true


    certificatesResolvers:
      http:
        acme:
          email: root@example.com
          storage: acme.json
          tlsChallenge: {}

    tls:
      options:
        default:
          minVersion: "VersionTLS12"
          sniStrict: true
          cipherSuites:
            - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305


Let's go over what each part of this file is doing.


This enables the Traefik WebUI which can help for debugging.

.. code-block:: yaml

    api:
      dashboard: true

This allows our backend services to have invalid TLS certificates.
This helps fix problems with reverse proxying applications that force
TLS on the whole chain (for example Nextcloud's 2FA stuff).

.. code-block:: yaml

    serversTransport:
      insecureSkipVerify: true

Now we configure our Traefik server's entrypoints. ``:80`` for http and
``:443`` for https.

.. code-block:: yaml

    entryPoints:
      http:
        address: ":80"
      https:
        address: ":443"

Thes settings configure our Traefik server's configuration providers.
The Docker provider allows Traefik to look at our running Docker containers
and create proxy rules basde on what it sees. The file provider allows
Traefik to look at another ``yaml`` file

.. code-block:: yaml

    providers:
      docker:
        endpoint: "unix:///var/run/docker.sock"
        exposedByDefault: false
      file:
        filename: home.yml
        watch: true

The ``certificatesResolvers`` section provides configuration for ACME.
By default this will use Let's Encrypt. The provided email address is
used for sending certificate expiration notifications. The ``tlsChallenge``
setting uses HTTPS for the certificate challenge with helps with passing
the challenge through the reverse proxy and VPN tunnel.

.. code-block:: yaml

    certificatesResolvers:
      http:
        acme:
          email: letsencrypt.notifications@example.com
          storage: acme.json
          tlsChallenge: {}

Finally we configure Traefik's TLS version and ciphersuite. An example
configuration is gives below. Mozilla's TLS configuration generator
tool (https://ssl-config.mozilla.org) can be used to generate these
settings. Note that the Mozilla tool will generate configuration for
Traefik in the ``toml`` markup langague. You will need to convert
this to ``yaml`` and fill it in below. This should be pretty simple
as this section contains minimal formatting.

.. code-block:: yaml

    tls:
      options:
        default:
          minVersion: "VersionTLS12"
          sniStrict: true
          cipherSuites:
            - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305


Traefik ``home.yaml`` Configuration File
||||||||||||||||||||||||||||||||||||||||

Next we will create the ``home.yaml`` file. This file explains to Traefik
how to forward connections to our home network. We will later configure the
VPN tunned such that our home server is reachable at ``10.7.1.2`` from our cloud
server. The contents should be as follows.


.. code-block:: yaml

    http:
      services:
        home:
          loadBalancer:
            servers:
              - url: "http://10.7.1.2"

    tcp:
      services:
        home-secure:
          loadBalancer:
            servers:
              - address: "10.7.1.2:443"

    routers:
      home:
        entryPoints:
          - "http"
        rule: "Host(`home.example.com`)"
        service: home

    routers:
      home-secure:
        entryPoints:
          - "https"
        rule: "HostSNI(`home.example.com`)"
        service: home-secure
        tls:
          passthrough: true


First we define our home server's VPN address as the http service ``home``.

.. code-block:: yaml

    http:
      services:
        home:
          loadBalancer:
            servers:
              - url: "http://10.7.1.2"

Next we define our home server's VPN address as an https service. This backend uses
the tcp router as we do not want to terminate the TLS connection on the
cloud server. No ``https`` in necessary here.

.. code-block:: yaml

     tcp:
      services:
        home-secure:
          loadBalancer:
            servers:
              - address: "10.7.1.2:443"

We then add a routing rule to forward http connections to the home backend.
This rule forwards all incoming requests on the http entry point to the
``http`` service which we defined above.

.. code-block:: yaml

    routers:
      home:
        entryPoints:
          - "http"
        rule: "Host(`home.example.com`)"
        service: home


We add a routing rule to forward https connections to the home backend.
This rule uses ``HostSNI`` and ``passthrough: true``. SNI allows us to
see what subdomain the request is for without terminating the connection.
Passthrough prevents the server from terminating the connection. This rule
forwards all incoming requests on the https entry point to the ``https``
service which we defined above.


.. code-block:: yaml

    routers:
      home-secure:
        entryPoints:
          - "https"
        rule: "HostSNI(`home.example.com`)"
        service: home-secure
        tls:
          passthrough: true


Miscellaneous Files
|||||||||||||||||||

Finally we will want to create the ``acme.json`` file. This file will be
empty for now. We only need to create the stub file so docker mounts it
correctly into the container.

.. code-block:: bash

   touch acme.json



Launch Traefik
______________

Now that we have all the configuration files in place for Traefik, we
need to launch it. We'll want to move back into the directory where
we created the ``docker-compose.yml`` file.

The following command will launch the Traefik Docker container using
Docker Compose. The container will launch in the background. Note that
this (and other Docker commands`` must either be run as root or by a user
who is in the ``docker`` group.

.. code-block:: bash

    docker-compose up -d


Now that we have Traefik launched we can open the WebUI and see if
everything we have set up so far is working as intended. You'll want
to open whatever URL you bound the WebUI to, in the case of this
tutorial this was ``cloud.example.com``.

When you open this webpage ideally you should see two things. The
first is that you are prompted for the login credentials you setup
earlier. The second is that you should eventually find yourself at
the web interface for Traefik. If this doesn't work, something has
gone wrong.

You will not have much to see in the web interface at this point.
We'll come back later to this once we add it some more rules.

From now on, Traefik should launch automatically when you reboot
the system. One requirement for this to work is for the Docker
daemon to start on boot as well. If this isn't configured, you'll
have to enable the system service for it. On systemd based systems
this can usually be done by running the following.

.. code-block:: bash

    sudo systemctl enable docker

Setting up Tinc
________________

Next we will need to setup the VPN connection between our cloud server
and our home server. This connection will be the tunnel that our reverse
proxy connections are sent over.

What is Tinc?
|||||||||||||

The software we will use to set this up is called Tinc. Tinc is, as
described by the Tinc website, "a Virtual Private Network (VPN) daemon
that uses tunnelling and encryption to create a secure private network
between hosts on the Internet."

Tinc is a really cool software that has a whole host of different uses.
In this case we are going to use Tinc to create a secure tunnel between
our cloud and home servers. This tunnel will not require us to open
port on our home network. It also ideally will automatically reconnect
in cases of power or internet failure ,assuming your systems can get
themselves back into a state where Tinc can do its thing.

Installing Tinc
|||||||||||||||

Ideally Tinc should be available on whatever Linux distro you have
decided to use for your cloud server. You will want to use your
package manager of choice to install Tinc. You can find more info
about using Tinc on your respective platform on the Tinc website at
http://tinc-vpn.org/download/

Configuring Tinc
||||||||||||||||


Now that we have Tinc installed we need to create configuration for it.

Tinc's configuration lives in ``/etc/tinc``. This folder is owned by root
and much of it will only be readable by the root user, so you'll want to
become the root user for this part. You can do this by running either

.. code-block:: bash

    sudo -s

or

.. code-block:: bash

    su

Now that you have become the root sure, you will first want move into
the tinc directory. In here, you will want to create a folder called
``tinc0`` where we will store the configuration for this tunnel. In
this folder, we will create 3 files, ``tinc.conf``, ``tinc-up``,
and ``tinc-down``. We will also create a folder called ``hosts``.


.. code-block:: bash

    touch tinc.conf tinc-up tinc-down
    chmod 600 *
    chmod +x tinc-up tinc-down
    mkdir hosts


Now that we have all these files created, let's populate them.


The file ``tinc.conf`` contains some overarching configuration for our Tinc
daemon.  This includes our device name and what networking driver Tinc
should use.


.. code-block:: ini

    Name = cloud
    Device = /dev/net/tun


The file ``tinc-up`` is an executable script that runs whenever this Tinc
interface is brought up. This script is used to configure the interface
using ``ip``. The interface is first brought up, then assigned an ip address.

.. code-block:: sh

    #!/bin/sh
    ip link set $INTERFACE up
    ip addr add 10.7.1.1/24 dev $INTERFACE


The file ``tinc-down`` is an executable script that runs whenever this Tinc
interface is brought down. It undoes the operations done in ``tinc-up``.

.. code-block:: sh

    #!/bin/sh
    ip addr del 10.7.1.1/24 dev $INTERFACE
    ip link set $INTERFACE down

Now we will create the host configuration file for our Tinc "hosts". These
configuration files denote settings that are shared to other hosts to enable
them to connect to the curernt host, in addition to also configuring the host
in question.

Having settings in ``tinc.conf`` and ``hosts`` can feel like we are
configuring settings in two different places for no reason. Think of
it as this, ``tinc.conf`` contains settings that are specific to the
Tinc configuration on the machine. The files in hosts contain
infomation that is useful to both the machine in question and other
machines that are connecting to that machine.


You will want to create a file in the hosts folder called ``cloud``. The contents
of the file should be as follows. You will need to fill in a port number of your
choosing.

.. code-block:: ini

    Address = cloud.example.com
    Port = port_number
    Subnet = 10.7.1.1/32


Finally we will generate the public and private keys for Tinc to use. Tinc
provides an nice utility to to this.

.. code-block:: bash

    tincd -n tinc0 -K

You will be prompted for file locations for the private key and the public
key. The default location for the private key should be fine. Make sure the
public key is being saved to the file we just created, ``/etc/tinc/tinc0/hosts/cloud``.



Part 3: Setting Up Your Home Server
-----------------------------------

Many of the instructions for configuring your home server will look
very similar to how we configured the cloud server. Note that there
will be subtle differences between the two that are very important
to building a functioning configuration.

Setting up Traefik
___________________

The first thing we are going to get setup on the home server is Traefik.

The first thing you want to do is create a directory for us to work in.
I would suggest naming it ``docker-compose`` or something similar. I like
to create sub-directories under this folder for each service I'm running
using Docker Compose. Inside this ``docker-compose`` folder you'll now
want to create a ``traefik`` folder.

Now that you have this directory structure setup, we can start configuring
your services.

``docker-compose.yaml`` Configuration
|||||||||||||||||||||||||||||||||||||

First, inside this traefik folder you will want to create a file called
``docker-compose.yaml``. The contents of the file should be as follows:

.. code-block:: yaml

    version: '3'

    services:
      traefik:
        image: traefik:latest
        container_name: traefik
        restart: unless-stopped
        networks:
          - proxy
        ports:
          - 80:80
          - 443:443
        volumes:
          - /etc/localtime:/etc/localtime:ro
          - /var/run/docker.sock:/var/run/docker.sock:ro
          - ./data/traefik.yml:/traefik.yml:ro
          - ./data/acme.json:/acme.json


    networks:
      proxy:
        external: true

Note that the ``home.yaml`` config file mount it not needed on the home
server. Also note that the labels that configure Traefik to allow us to
access the web interface are also not needed.


Traefik Configuration File
||||||||||||||||||||||||||

Next we will need to create some of the files that we are mounting into the
traefik container. The first thing you will need to do is create a ``data``
folder that we will use to store the rest of the Traefik configuration.

Now that we made the ``data`` folder we can create the required traefik
configuration files.

First we want to create ``traefik.yml``. The following is the full contents
of the file. This configuration should look very similar to the configuration
we did on our cloud server. The only difference is the lack of the
``home.yaml`` configuration file provider.


.. code-block:: yaml

    serversTransport:
      insecureSkipVerify: true

    entryPoints:
      http:
        address: ":80"
      https:
        address: ":443"

    providers:
      docker:
        endpoint: "unix:///var/run/docker.sock"
        exposedByDefault: false


    certificatesResolvers:
      http:
        acme:
          email: root@example.com
          storage: acme.json
          tlsChallenge: {}

    tls:
      options:
        default:
          minVersion: "VersionTLS12"
          sniStrict: true
          cipherSuites:
            - TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
            - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
            - TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305
            - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305


Miscellaneous Files
|||||||||||||||||||

Finally we will want to create the ``acme.json`` file. This file will be
empty for now. We only need to create the stub file so docker mounts it
correctly into the container.

.. code-block:: bash

   touch acme.json



Launch Traefik
______________

Now that we have all the configuration files in place for Traefik, we
need to launch it. We'll want to move back into the directory where
we created the ``docker-compose.yml`` file.

The following command will launch the Traefik Docker container using
Docker Compose. The container will launch in the background. Note that
this (and other Docker commands`` must either be run as root or by a user
who is in the ``docker`` group.

.. code-block:: bash

    docker-compose up -d


Now that we have Traefik launched we can open the WebUI and see if
everything we have set up so far is working as intended. You'll want
to open whatever URL you bound the WebUI to, in the case of this
tutorial this was ``home.example.com``.

When you open this webpage ideally you should see two things. The
first is that you are prompted for the login credentials you setup
earlier. The second is that you should eventually find yourself at
the web interface for Traefik. If this doesn't work, something has
gone wrong.

You will not have much to see in the web interface at this point.
We'll come back later to this once we add it some more rules.

From now on, Traefik should launch automatically when you reboot
the system. One requirement for this to work is for the Docker
daemon to start on boot as well. If this isn't configured, you'll
have to enable the system service for it. On systemd based systems
this can usually be done by running the following.

.. code-block:: bash

    sudo systemctl enable docker

Setting up Tinc
________________

Next we will need to setup the VPN connection between our cloud server
and our home server. This connection will be the tunnel that our reverse
proxy connections are sent over.

Installing Tinc
|||||||||||||||

Ideally Tinc should be available on whatever Linux distro you have
decided to use for your home server. You will want to use your
package manager of choice to install Tinc. You can find more info
about using Tinc on your respective platform on the Tinc website at
http://tinc-vpn.org/download/

Configuring Tinc
||||||||||||||||

Now that we have Tinc installed we need to create configuration for it.

Tinc's configuration lives in ``/etc/tinc``. This folder is owned by root
and much of it will only be readable by the root user, so you'll want to
become the root user for this part. You can do this by running either

.. code-block:: bash

    sudo -s

or

.. code-block:: bash

    su

Now that you have become the root sure, you will first want move into
the tinc directory. In here, you will want to create a folder called
``tinc0`` where we will store the configuration for this tunnel. In
this folder, we will create 3 files, ``tinc.conf``, ``tinc-up``,
and ``tinc-down``. We will also create a folder called ``hosts``.


.. code-block:: bash

    touch tinc.conf tinc-up tinc-down
    chmod 600 *
    chmod +x tinc-up tinc-down
    mkdir hosts


Now that we have all these files created, let's populate them.


The file ``tinc.conf`` contains some overarching configuration for our Tinc
daemon.  This includes our device name and what networking driver Tinc
should use.


.. code-block:: ini

    Name = home
    ConnectTo = cloud
    Device = /dev/net/tun


The file ``tinc-up`` is an executable script that runs whenever this Tinc
interface is brought up. This script is used to configure the interface
using ``ip``. The interface is first brought up, then assigned an ip address.

.. code-block:: sh

    #!/bin/sh
    ip link set $INTERFACE up
    ip addr add 10.7.1.2/24 dev $INTERFACE


The file ``tinc-down`` is an executable script that runs whenever this Tinc
interface is brought down. It undoes the operations done in ``tinc-up``.

.. code-block:: sh

    #!/bin/sh
    ip addr del 10.7.1.2/24 dev $INTERFACE
    ip link set $INTERFACE down

Now we will create the host configuration file for our Tinc "hosts". These
configuration files denote settings that are shared to other hosts to enable
them to connect to the curernt host, in addition to also configuring the host
in question.

Having settings in ``tinc.conf`` and ``hosts`` can feel like we are
configuring settings in two different places for no reason. Think of
it as this, ``tinc.conf`` contains settings that are specific to the
Tinc configuration on the machine. The files in hosts contain
infomation that is useful to both the machine in question and other
machines that are connecting to that machine.


You will want to create a file in the hosts folder called ``home``. The contents
of the file should be as follows. You will need to fill in a port number of your
choosing.

.. code-block:: ini

    Address = home.example.com
    Port = port_number
    Subnet = 10.7.1.2/32
    Subnet = 10.7.0.0/24


Finally we will generate the public and private keys for Tinc to use. Tinc
provides an nice utility to to this.

.. code-block:: bash

    tincd -n tinc0 -K

You will be prompted for file locations for the private key and the public
key. The default location for the private key should be fine. Make sure the
public key is being saved to the file we just created, ``/etc/tinc/tinc0/hosts/home``.
